## ministock-management-app

> Web Client para a API Restful [ministock-management](https://github.com/Hguimaraes/ministock-management).

### Como utilizar

1. Instale a última versão do NodeJS [aqui](https://nodejs.org/en/download/)
2. Clone este repositório
3. Na linha de comando, dentro do diretório deste repositório digite:

```bash
npm install bower -g
npm install
bower install
node index.js
```

E a aplicação será iniciada na porta 3000. Partimos do suposto que a API está no endereço: *"http://localhost:52794/"*.
Para mudar esse endereço, edite o arquivo *app.js* no caminho "public/js/app.js".