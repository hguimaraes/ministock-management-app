angular.module("ministockApp")
  .controller("OrdemCtrl", function ($scope, $routeParams, $location, $http) {
    
    // Initialize some variables
    $scope.searchResult = false;
    $scope.searchResultData = null;
    $scope.date = null;
    var route_controller = "api/Ordem";
    
    $scope.searchByDate = function(){
      var req = null

      // If there is no data at the date field, search all
      if (!$scope.date) {
        req = {
          method: 'GET',
          url: API_ADDRESS + route_controller
        };
    
      // Otherwise, search by date
      } else {
        req = {
          method: 'GET',
          url: API_ADDRESS + route_controller + "/data/" + $scope.date
        };
      }

      // Send a GET request
      $http(req).then(function(response){
        $scope.searchResultData = response.data;
      }, function(err){
        Materialize.toast('Não foi possível encontrar resultados para a busca!', 4000);
      });
    };
  });