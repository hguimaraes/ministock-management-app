angular.module("ministockApp")
  .controller("HomeCtrl", function ($scope, $routeParams, $location, $http) {
    
    // Help variables
    var route_controller = "api/Ativo";
    $scope.ativos = null;
    $scope.ativosPos = null;

    $scope.trade = function(opts){
      // Check if all the fields contains some value
      if(!opts.preco || !opts.quantidade || !opts.classe_negociacao) {
        Materialize.toast('Nenhum dos campos pode estar vazio!', 4000);
        return;
      }

      var req = {
        method: 'POST',
        url: API_ADDRESS + "api/Ordem",
        data: {
          fk_id_ativo: opts.id_ativo,
          quantidade: opts.quantidade,
          preco: opts.preco,
          classe_negociacao: opts.classe_negociacao,
          data: new Date()
        }
      };

      $http(req).then(function(response){
        // Get Posicao of Ativos
        getPosicao($scope.ativos);
        Materialize.toast('Operação realizada com Sucesso', 4000);
      }, function(err){
        if (err.status == 400) {
          Materialize.toast('Erro no campo Quantidade ou Operação!', 4000);
        } else {
          Materialize.toast('Erro na Operação, confira o console para informações', 4000);
          console.log(err);
        }
      });
    }

    var getDescById = function(x, id) {
      for (var i = 0; i < x.length; i++) {
        if (x[i].id_ativo == id) {
          return x[i];
        }
      }
    }

    var getPosicao = function(ativos){
      $scope.ativosPos = []

      for (var i = 0; i < ativos.length; i++) {
        var req = {
          method: 'GET',
          url: API_ADDRESS + "api/Ordem/posicao/" + ativos[i].id_ativo
        };

        // Send a GET request
        $http(req).then(function(response){
          id = response.data.Id;
          desc = getDescById(ativos, id);

          $scope.ativosPos.push({
            'desc': desc.descricao,
            'posicao': response.data.Pos
          });
        }, function(err){
          Materialize.toast('Erro atualizar a Posição!', 4000);
        });
      }
    }

    var init = function(){
      var req = {
        method: 'GET',
        url: API_ADDRESS + route_controller
      };

      // Send a GET request
      $http(req).then(function(response){
        $scope.ativos = response.data;
        
        // Get Posicao of Ativos
        getPosicao($scope.ativos);
      }, function(err){
        Materialize.toast('Não foi possível encontrar a lista de Ativo! Verifique o status da API', 4000);
      });
    };
    init();
  });