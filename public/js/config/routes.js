angular.module('ministockApp')
  .config(function ($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true)
    $routeProvider
      .when('/', {
        templateUrl: '/views/home.html',
        controller: 'HomeCtrl'
      })
      .when('/ordem', {
        templateUrl: '/views/ordem.html',
        controller: 'OrdemCtrl'
      })
      .otherwise({
        templateUrl: '/views/error.html'
      })
  });